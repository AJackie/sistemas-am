<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!--Según el video debería ser el boostrap 3.7 pero yo tengo el 4.2.x-->
	<!--Login Responsivo link: https://www.youtube.com/watch?reload=9&v=Tc47wdpZI1w-->
	<!--Boostrap-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/login.css">

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<!--AJAX -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<title>Document</title>
</head>
<body>
	<div class="container-fluid bg">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12"></div>
				<div class="col-md-4 col-sm-4 col-xs-12">
				<!--Inicio del formulario LogIn-->
				<form class="form-login">
				<h1>Iniciar Sesión</h1>
				  <div class="form-group">
				    <label for="matricula">Matricula</label>
				    <input type="text" class="form-control" id="matricula" name="matricula" placeholder="Escribe tu matricula">
				  </div>
				  <div class="form-group">
				    <label for="password">Contraseña</label>
				    <input type="password" name="password" class="form-control" id="contrasenia" placeholder="Contraseña">
				  </div>
				  <div class="form-group form-check">
				    <input type="checkbox" class="form-check-input" id="recordarme">
				    <label class="form-check-label" >Recuerdame
				    </label>
				  </div>
				  <button type="submit" class="btn btn-success btn-block">Submit</button>
				</form>
				<!--Fin del formulario LogIn-->	
				</div>				
			<div class="col-md-4 col-sm-4 col-xs-12"></div>
		</div>
	</div>
</body>
</html>