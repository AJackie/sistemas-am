<?php

require_once "../modelo/conexion.php";
/**
 * 
 */
require_once "../modelo/objetos/materiales.consulta.php";

class MaterialesControladores
{
	function __construct()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			if (!empty($_POST['nuevo_tipo'])) {
				$nuevo_tipo = $_POST['nuevo_tipo'];
				MaterialesModelo::nuevo_tipo($nuevo_tipo);
			}
		}
	}

}


require_once "../vistas/altaTiposMateriales.php";

?>