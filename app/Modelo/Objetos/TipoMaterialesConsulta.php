<?php

include "../ConexionDB.php";
/**
 * 
 */

class TipoMaterialConsulta
{

	function conexion(){
		$con = new ConexionDB();
		$conexion = $con->obtenerConexion();
		return $conexion;
	}
	function getAll(){
		
		$sql = "SELECT * FROM tipos_material";
		$stm = self::conexion()->prepare($sql);
		$stm->execute();

		$resultado = $stm->fetchAll(PDO::FETCH_ASSOC);

		return $resultado;
	}

	function nuevoTipo($nuevo_tipo){
		$sql = "INSERT INTO tipos_material(tipo_m) VALUES (?)";
		$stm = self::conexion()->prepare($sql);
		$r = $stm->execute(array($nuevo_tipo));

		return "nuevo tipo agregado ";
	}

	function buscarid($tipo){
		$sql = "SELECT idtipos_material FROM tipos_material WHERE tipo_m = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($tipo));

		$resultado = $stm->fetch(PDO::FETCH_ASSOC);

		return $resultado;

	}

	function eliminar($id){
		$sql = "DELETE FROM tipos_material WHERE idtipos_material = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($id));

		return "Objeto eliminado";
	}

	function actualizar($tipo, $id){
		$sql = "UPDATE tipos_material SET tipo_m = (?) WHERE idtipos_material = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($tipo, $id));

		return "Actualización exitosa";
	}
}


/**
 * guardado de prueba
 */

?>