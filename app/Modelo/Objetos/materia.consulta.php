<?php
	
	require_once "../../controladores/configPA.php";
	
	public Class modeloMateria{
		public function buscarMateria($clave_curso){
			$sql = "SELECT *  FROM materia WHERE clave = ?";

			$consulta = $con->prepare($sql);

			$consulta->execute(array($clave_curso));

			$resultado = $consulta->fetch(PDO::FETCH_ASSOC);

			return $resultado;
		}

		public function crearMateria($clave_curso, $curso){
			$sql = "INSERT INTO materia(clave, materia) VALUES (?,?)";

			$consulta = $con->prepare($sql);

			$consulta->execute(array($clave_curso, $curso));
		}

	}	
?>