<?php
include "../ConexionDB.php";
/**
 * 
 */

class EstadoMaterialConsulta
{

	function conexion(){
		$con = new ConexionDB();
		$conexion = $con->obtenerConexion();
		return $conexion;
	}
	function getAll(){
		
		$sql = "SELECT * FROM estado_material";
		$stm = self::conexion()->prepare($sql);
		$stm->execute();

		$resultado = $stm->fetchAll(PDO::FETCH_ASSOC);

		return $resultado;
	}

	function nuevoEstado($nuevo_tipo){
		$sql = "INSERT INTO estado_material(estado_material) VALUES (?)";
		$stm = self::conexion()->prepare($sql);
		$r = $stm->execute(array($nuevo_tipo));

		return "nuevo tipo agregado ";
	}

	function buscarid($tipo){
		$sql = "SELECT idestado_material FROM estado_material WHERE idestado_material = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($tipo));

		$resultado = $stm->fetch(PDO::FETCH_ASSOC);

		return $resultado;

	}

	function eliminar($id){
		$sql = "DELETE FROM estado_material WHERE idestado_material = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($id));

		return "Objeto eliminado";
	}

	function actualizar($tipo, $id){
		$sql = "UPDATE estado_material SET estado_material = (?) WHERE idestado_material = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($tipo, $id));

		return "Actualización exitosa";
	}
}

?>