<?php
namespace App/Modelo/Objetos;

use App/Modelo/ConexionDB.php;
/**
 * Pendiente por revisar
 */

class MaterialesConsulta
{

	function conexion(){
		$con = new ConexionDB();
		$conexion = $con->obtenerConexion();
		return $conexion;
	}
	function getAll(){
		
		$sql = "SELECT 
		materiales.nombre, 
		modelo.materiales, 
		tipos_material.tipo_m
		estado_material.estado_material,
		FROM materiales 
		INNER JOIN tipos_material ON modelo.tipo_material = tipos_material.idtipos_material
		INNER JOIN estado_material ON modelo.estado_material_id = estado_material.idestado_material";
		$stm = self::conexion()->prepare($sql);
		$stm->execute();

		$resultado = $stm->fetchAll(PDO::FETCH_ASSOC);

		return $resultado;
	}

	function nuevoEstado($nombre, $modelo, $tipo_material, $estado_material_id){
		$sql = "INSERT INTO materiales(nombre,modelo,tipo_material,estado_material_id) VALUES (?,?,?,?,?)";
		$stm = self::conexion()->prepare($sql);
		$r = $stm->execute(array($nombre, $modelo, $tipo_material, $estado_material_id));

		return "nuevo tipo agregado ";
	}

	function buscarMaterial($nombre, $modelo, $tipo_material, $estado_material_id){
		$sql = "SELECT idestado_material FROM estado_material WHERE nombre = ? AND modelo = ? AND tipo_material = ? AND estado_material_id = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($tipo));

		$resultado = $stm->fetch(PDO::FETCH_ASSOC);

		return $resultado;

	}

	function eliminar($id){
		$sql = "DELETE FROM estado_material WHERE idestado_material = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($id));

		return "Objeto eliminado";
	}

	function actualizar($id, $nombre, $modelo, $tipo_material, $estado_material_id){
		$sql = "UPDATE estado_material SET nombre = ? AND modelo = ? AND tipo_material = ? AND estado_material = ? WHERE idestado_material = ?";
		$stm = self::conexion()->prepare($sql);
		$stm->execute(array($nombre, $modelo, $tipo_material, $estado_material_id, $id));

		return "Actualización exitosa";
	}
}
?>